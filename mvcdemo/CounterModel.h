//
//  CounterModel.h
//  mvcdemo
//
//  Created by James Cash on 13-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CounterModel : NSObject

@property (nonatomic,strong) NSMutableArray<NSNumber*>* numbers;

- (NSInteger)valueOfCounter:(NSInteger)idx;
- (void)incrementCounter:(NSInteger)idx;

@end
