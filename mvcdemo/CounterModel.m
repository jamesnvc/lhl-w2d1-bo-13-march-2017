//
//  CounterModel.m
//  mvcdemo
//
//  Created by James Cash on 13-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "CounterModel.h"

@implementation CounterModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _numbers = [NSMutableArray arrayWithArray:@[ @(1), @(2), @(3) ]];
    }
    return self;
}

- (NSInteger)valueOfCounter:(NSInteger)idx
{
    return self.numbers[idx].integerValue;
}

- (void)incrementCounter:(NSInteger)idx
{
    NSInteger oldVal = [self valueOfCounter:idx];
    self.numbers[idx] = @(oldVal + 1);
}

@end
