//
//  ViewController.m
//  mvcdemo
//
//  Created by James Cash on 13-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CounterModel.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray<UILabel*>* counterLabels;

@property (strong, nonatomic) CounterModel *model;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.model = [[CounterModel alloc] init];
    [self updateLabels];
}

- (void)updateLabels {
    self.counterLabels[0].text = self.model.numbers[0].stringValue;
    self.counterLabels[1].text = self.model.numbers[1].stringValue;
    self.counterLabels[2].text = self.model.numbers[2].stringValue;
//    self.counterLabels[2].text = [NSString stringWithFormat:@"%ld", [self.model valueOfCounter:2]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)incrementCounter:(UIButton *)sender {
    NSLog(@"Button # %ld was pressed", (long)sender.tag);
    [self.model incrementCounter:sender.tag - 1];
    [self updateLabels];
}

@end
